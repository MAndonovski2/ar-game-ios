//
//  ARGameSession+Gestures.swift
//  Orlinski AR Game
//
//  Created by Martin on 1.9.20.
//  Copyright © 2020 Martin. All rights reserved.
//


import UIKit
import RealityKit
import ARKit

extension ARGameSessionViewController: UIGestureRecognizerDelegate {
    
    // MARK: - UI Gestures and Touches
    @IBAction func handleTap(_ gesture: UITapGestureRecognizer) {
        guard let gameBoard = gameBoard else { return }
        
        if sessionState == .adjustingBoard {
            UIApplication.shared.isIdleTimerDisabled = true

            arView = ARView(frame: self.view.frame)
            arView?.session.delegate = self
            arView?.automaticallyConfigureSession = false
            arView?.session = sessionARView.session
            arView?.debugOptions = .showStatistics
            self.view.addSubview(arView!)
            
            let boardAnchor = ARAnchor(name: "Board Anchor", transform: gameBoard.simdWorldTransform)
            arView?.session.add(anchor: boardAnchor)
            sessionState = .boardIsPlaced
            
            DispatchQueue.main.async {
                self.sessionARView = nil
            }
            
            //        sessionIDObservation = observe(\.sessionARView.session.identifier, options: [.new]) { object, change in
            //            print("SessionID changed to: \(change.newValue!)")
            //            // Tell all other peers about your ARSession's changed ID, so
            //            // that they can keep track of which ARAnchors are yours.
            //            guard let multipeerSession = self.multipeerSession else { return }
            //            self.sendARSessionIDTo(peers: multipeerSession.connectedPeers)
            //        }
            //        Start looking for other players via MultiPeerConnectivity.

            //        multipeerSession = MultipeerSession(receivedDataHandler: receivedData, peerJoinedHandler:
            //                                            peerJoined, peerLeftHandler: peerLeft, peerDiscoveredHandler: peerDiscovered)
        }
    }
    
    @IBAction func handlePinch(_ gesture: ThresholdPinchGestureRecognizer) {
        guard canAdjustBoard, let gameBoard = gameBoard else { return }
        
        sessionState = .adjustingBoard
        
        switch gesture.state {
        case .changed where gesture.isThresholdExceeded:
            gameBoard.scale(by: Float(gesture.scale))
            gesture.scale = 1
        default:
            break
        }
    }
    
    @IBAction func handleRotation(_ gesture: ThresholdRotationGestureRecognizer) {
        guard canAdjustBoard, let gameBoard = gameBoard else { return }
        
        sessionState = .adjustingBoard
        
        switch gesture.state {
        case .changed where gesture.isThresholdExceeded:
            if gameBoard.eulerAngles.x > .pi / 2 {
                gameBoard.simdEulerAngles.y += Float(gesture.rotation)
            } else {
                gameBoard.simdEulerAngles.y -= Float(gesture.rotation)
            }
            gesture.rotation = 0
        default:
            break
        }
    }
    
    @IBAction func handlePan(_ gesture: ThresholdPanGestureRecognizer) {
        guard canAdjustBoard, let gameBoard = gameBoard else { return }
        
        sessionState = .adjustingBoard
        
        let location = gesture.location(in: sessionARView)
        let results = sessionARView.hitTest(location, types: .existingPlane)
        guard let nearestPlane = results.first else {
            return
        }
        
        switch gesture.state {
        case .began:
            panOffset = nearestPlane.worldTransform.columns.3.xyz - gameBoard.simdWorldPosition
        case .changed:
            gameBoard.simdWorldPosition = nearestPlane.worldTransform.columns.3.xyz - panOffset
        default:
            break
        }
    }
    
    @IBAction func handleLongPress(_ gesture: UILongPressGestureRecognizer) {
        guard canAdjustBoard, let gameBoard = gameBoard else { return }
        
        sessionState = .adjustingBoard
        gameBoard.useDefaultScale()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        touch(type: .began)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touch(type: .ended)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touch(type: .ended)
    }

    private func touch(type: TouchType) {
//        gameManager?.handleTouch(type)
    }

    func gestureRecognizer(_ first: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith second: UIGestureRecognizer) -> Bool {
        if first is UIRotationGestureRecognizer && second is UIPinchGestureRecognizer {
            return true
        } else if first is UIRotationGestureRecognizer && second is UIPanGestureRecognizer {
            return true
        } else if first is UIPinchGestureRecognizer && second is UIRotationGestureRecognizer {
            return true
        } else if first is UIPinchGestureRecognizer && second is UIPanGestureRecognizer {
            return true
        } else if first is UIPanGestureRecognizer && second is UIPinchGestureRecognizer {
            return true
        } else if first is UIPanGestureRecognizer && second is UIRotationGestureRecognizer {
            return true
        }
        return false
    }
}
