//
//  ARGameSessionViewController+ARSessionDelegate.swift
//  Orlinski AR Game
//
//  Created by Martin on 2.9.20.
//  Copyright © 2020 Martin. All rights reserved.
//

import ARKit
import RealityKit

extension ARGameSessionViewController: ARSessionDelegate {
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        updateGameBoard(frame: frame)
    }
    
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        for anchor in anchors {
            if anchor.name == "Board Anchor" {
                let anchorEntity = AnchorEntity(anchor: anchor)
                if let entity = try? Entity.load(named: "gameassets.scnassets/scene/testScene.usdz") {
                    anchorEntity.addChild(entity)
                    arView?.scene.addAnchor(anchorEntity)
                    entity.playAnimation(named: "\(entity.availableAnimations.first!.name!)")
                }
            }
        }
    }
}
