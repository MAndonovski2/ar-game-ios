//
//  ARGameSessionViewController+ARSCNDelegate.swift
//  Orlinski AR Game
//
//  Created by Martin on 31.8.20.
//  Copyright © 2020 Martin. All rights reserved.
//

import ARKit
import os.log

extension ARGameSessionViewController: ARSCNViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        print("\(self.classForCoder)/" + #function + ", anchor id: \(anchor.identifier)")
        guard let planeAnchor = anchor as? ARPlaneAnchor else {fatalError()}

        let planeGeometry = ARSCNPlaneGeometry(device: device)!
        planeGeometry.update(from: planeAnchor.geometry)
        
        let color = planeAnchor.alignment == .horizontal ? UIColor.blue : UIColor.green
        planeAnchor.addPlaneNode(on: node, geometry: planeGeometry, contents: color.withAlphaComponent(0.5))
    }

    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else {fatalError()}
        if let planeGeometry = planeAnchor.findShapedPlaneNode(on: node)?.geometry as? ARSCNPlaneGeometry {
            planeGeometry.update(from: planeAnchor.geometry)
        }
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        os_log(.info, "camera tracking state changed to %s", "\(camera.trackingState)")
//        DispatchQueue.main.async {
//            self.trackingStateLabel.text = "\(camera.trackingState)"
//        }
//
//        switch camera.trackingState {
//        case .normal:
//            // Resume game if previously interrupted
//            if isSessionInterrupted {
//                isSessionInterrupted = false
//            }
//
//            // Fade in the board if previously hidden
//            if gameBoard.isHidden {
//                gameBoard.opacity = 1.0
//                gameBoard.isHidden = false
//            }
//
//            // Fade in the level if previously hidden
//            if renderRoot.opacity == 0.0 {
//                renderRoot.opacity = 1.0
//                assert(!renderRoot.isHidden)
//            }
//        case .limited:
//            // Hide the game board and level if tracking is limited
//            gameBoard.isHidden = true
//            renderRoot.opacity = 0.0
//        default:
//            break
//        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Get localized strings from error
        let errorWithInfo = error as NSError
        let messages = [
            errorWithInfo.localizedDescription,
            errorWithInfo.localizedFailureReason,
            errorWithInfo.localizedRecoverySuggestion
        ]
        
        // Use `compactMap(_:)` to remove optional error messages.
        let errorMessage = messages.compactMap({ $0 }).joined(separator: "\n")
        
        // Present the error message to the user
        showAlert(title: "Session Error", message: errorMessage, actions: nil)
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        os_log(.info, "[sessionWasInterrupted] --  %s", "\(sessionState)")
//
//        // Inform the user that the session has been interrupted
//        isSessionInterrupted = true
//
//        // Hide game board and level
//        gameBoard.isHidden = true
//        renderRoot.opacity = 0.0
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        os_log(.info, "[sessionInterruptionEnded] --  %s", "\(sessionState)")
    }
    
    func sessionShouldAttemptRelocalization(_ session: ARSession) -> Bool {
        return true
    }
}
