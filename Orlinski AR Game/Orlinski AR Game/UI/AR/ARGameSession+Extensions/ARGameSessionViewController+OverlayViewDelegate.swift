//
//  ARGameSessionViewController+OverlayViewDelegate.swift
//  Orlinski AR Game
//
//  Created by Martin on 31.8.20.
//  Copyright © 2020 Martin. All rights reserved.
//

import ARKit

extension ARGameSessionViewController: ARCoachingOverlayViewDelegate{
    func setOverlay(automatically: Bool, forDetectionType goal: ARCoachingOverlayView.Goal){
        
        //1. Link The GuidanceOverlay To Our Current Session
        self.guidanceOverlay.session = self.sessionARView.session
        self.guidanceOverlay.delegate = self
        self.sessionARView.addSubview(self.guidanceOverlay)

        //2. Set It To Fill Our View
        NSLayoutConstraint.activate([
        NSLayoutConstraint(item:  guidanceOverlay, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0),
        NSLayoutConstraint(item:  guidanceOverlay, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0),
        NSLayoutConstraint(item:  guidanceOverlay, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0),
        NSLayoutConstraint(item:  guidanceOverlay, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
        ])

        guidanceOverlay.translatesAutoresizingMaskIntoConstraints = false

        //3. Enable The Overlay To Activate Automatically Based On User Preference
        self.guidanceOverlay.activatesAutomatically = automatically

        //4. Set The Purpose Of The Overlay Based On The User Preference
        self.guidanceOverlay.goal = goal
    }
    
    //1. Called When The ARCoachingOverlayView Is Active And Displayed
    func coachingOverlayViewWillActivate(_ coachingOverlayView: ARCoachingOverlayView) { }

    //2. Called When The ARCoachingOverlayView Is No Active And No Longer Displayer
    func coachingOverlayViewDidDeactivate(_ coachingOverlayView: ARCoachingOverlayView) { }

    //3. Called When Tracking Conditions Are Poor Or The Seesion Needs Restarting
    func coachingOverlayViewDidRequestSessionReset(_ coachingOverlayView: ARCoachingOverlayView) { }

}
