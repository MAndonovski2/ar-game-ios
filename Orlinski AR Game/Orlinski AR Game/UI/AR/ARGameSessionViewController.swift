//
//  ARGameSessionViewController.swift
//  Orlinski AR Game
//
//  Created by Martin on 8/11/20.
//  Copyright © 2020 Martin. All rights reserved.
//

import UIKit
import ARKit
import RealityKit
import os.signpost
import MultipeerConnectivity

class ARGameSessionViewController: UIViewController {
    @IBOutlet weak var sessionARView: ARSCNView!
    @IBOutlet weak var messageLabel: MessageLabel!
    
    var planeNodes: [SCNNode] = []
    var arView: ARView?
    let configuration = ARWorldTrackingConfiguration()
    
    let device = MTLCreateSystemDefaultDevice()!
    let guidanceOverlay = ARCoachingOverlayView()
    
    var targetWorldMap: ARWorldMap?
    var gameBoard: GameBoard?
    var panOffset = SIMD3<Float>()
    
    //var multipeerSession: MultipeerSession?
    var sessionIDObservation: NSKeyValueObservation?
    var peerSessionIDs = [MCPeerID: String]()
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    override var shouldAutorotate: Bool { return true }
    
    var screenCenter: CGPoint {
        let bounds = sessionARView.bounds
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    var attemptingBoardPlacement: Bool {
        return sessionState == .lookingForSurface || sessionState == .placingBoard
    }
    
    var canAdjustBoard: Bool {
        return sessionState == .placingBoard || sessionState == .adjustingBoard
    }
    
    var sessionState: Game.SessionState = .setup {
        didSet {
            guard oldValue != sessionState else { return }

            os_log(.info, "session state changed to %s", "\(sessionState)")
            configureSceneView()
            configureARSession()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        gameBoard = GameBoard()
        
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        AppDelegate.AppUtility.lockOrientation(.allButUpsideDown)
        sessionARView.session.pause()
        planeNodes = []
        gameBoard = nil
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func hostGamePressed(_ sender: Any) {
        sessionState = .lookingForSurface
    }
    
    @IBAction func joinGamePressed(_ sender: Any) {
    }
    
    
    private func configureARSession() {
        guard let gameBoard = gameBoard else { return }
        let configuration = ARWorldTrackingConfiguration()
//        configuration.environmentTexturing = .automatic
//        configuration.frameSemantics = .personSegmentationWithDepth
        configuration.initialWorldMap = targetWorldMap
        configuration.planeDetection = [.horizontal]
        
        if let message = sessionState.localizedInstruction {
            message.showAsToast(on: self.view)
        }
        
        switch sessionState {
        case .setup:
            os_log(.info, "AR session paused")
            sessionARView.session.pause()
            return
        case .lookingForSurface, .waitingForBoard:
            configuration.planeDetection = [.horizontal]
            if sessionARView.isPlaying {
                return
            }
        case .localizingToBoard:
            guard let targetWorldMap = targetWorldMap else { os_log(.error, "should have had a world map"); return }
            DispatchQueue.main.async {
                gameBoard.anchor = targetWorldMap.boardAnchor
                if let boardAnchor = gameBoard.anchor {
                    gameBoard.simdTransform = boardAnchor.transform
                    gameBoard.simdScale = SIMD3<Float>(repeating: Float(boardAnchor.size.width))
                }
                gameBoard.hideBorder(duration: 0)
            }
        case .setupStage, .placingBoard, .adjustingBoard, .gameInProgress, .boardIsPlaced, .sidePicking:
            return
        }
        
//        configuration.isLightEstimationEnabled = false
//        configuration.isCollaborationEnabled = true
        
        os_log(.info, "configured AR session")
        sessionARView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        sessionARView.session.delegate = self
        
        // Use key-value observation to monitor your ARSession's identifier.
    }
    
    func loadWorldMap(from archivedData: Data) {
        do {
            let uncompressedData = try archivedData.decompressed()
            guard let worldMap = try NSKeyedUnarchiver.unarchivedObject(ofClass: ARWorldMap.self, from: uncompressedData) else {
                os_log(.error, "The WorldMap received couldn't be read")
                DispatchQueue.main.async {
                    AlertManager.shared.showAlert(self, title: "An error occured while loading the WorldMap (Failed to read)")
                    self.sessionState = .setup
                }
                return
            }
            
            DispatchQueue.main.async {
                self.targetWorldMap = worldMap
                self.sessionState = .localizingToBoard
            }
        } catch {
            os_log(.error, "The WorldMap received couldn't be decompressed")
            DispatchQueue.main.async {
                AlertManager.shared.showAlert(self, title: "An error occured while loading the WorldMap (Failed to decompress)")
                self.sessionState = .setup
            }
        }
    }
    
    func configureSceneView(){
        setOverlay(automatically: true, forDetectionType: .horizontalPlane)
        sessionARView.showsStatistics = true
        sessionARView.preferredFramesPerSecond = 60
    }
    
    func configureLighting(){
        sessionARView.automaticallyUpdatesLighting = true
        sessionARView.autoenablesDefaultLighting = true
    }
    
    func updateGameBoard(frame: ARFrame) {
        if sessionState == .setupStage {
            // this will advance the session state
//            setupLevel()
            return
        }
        
        // Only automatically update board when looking for surface or placing board
        guard attemptingBoardPlacement else {
            return
        }
        
        // Make sure this is only run on the render thread
        
        if gameBoard?.parent == nil, let gameBoard = gameBoard {
            self.sessionARView.scene.rootNode.addChildNode(gameBoard)
        }
        
        // Perform hit testing only when ARKit tracking is in a good state.
        if case .normal = frame.camera.trackingState {
            
            if let result = sessionARView.hitTest(screenCenter, types: [.estimatedHorizontalPlane, .existingPlaneUsingExtent]).first {
                // Ignore results that are too close to the camera when initially placing
                guard result.distance > 0.5 || sessionState == .placingBoard else { return }
                
                sessionState = .placingBoard
                gameBoard?.update(with: result, camera: frame.camera)
            } else {
                sessionState = .lookingForSurface
                if !(gameBoard?.isBorderHidden ?? true) {
                    gameBoard?.hideBorder()
                }
            }
        }
    }
}
