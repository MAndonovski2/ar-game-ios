//
//  AlertManager.swift
//  Orlinski AR Game
//
//  Created by Martin on 1.9.20.
//  Copyright © 2020 Martin. All rights reserved.
//

import UIKit

class AlertManager {
    static let shared = AlertManager()
    
    private init() {}
    
    func showAlert(_ controller: UIViewController,
                   title: String,
                   message: String? = nil,
                   actions: [UIAlertAction]? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let actions = actions {
            actions.forEach { alertController.addAction($0) }
        } else {
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        }
        controller.present(alertController, animated: true, completion: nil)
    }
}
