//
//  UIColor+Extension.swift
//  Orlinski AR Game
//
//  Created by Martin on 31.8.20.
//  Copyright © 2020 Martin. All rights reserved.
//

import UIKit

extension UIColor {
    open class var transparentWhite: UIColor {
        return UIColor.white.withAlphaComponent(0.20)
    }
}
