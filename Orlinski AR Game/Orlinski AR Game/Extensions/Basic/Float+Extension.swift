//
//  Float+Extension.swift
//  Orlinski AR Game
//
//  Created by Martin on 31.8.20.
//  Copyright © 2020 Martin. All rights reserved.
//

import Foundation

extension Float {
    func normalizedAngle(forMinimalRotationTo angle: Float, increment: Float) -> Float {
        var normalized = self
        while abs(normalized - angle) > increment / 2 {
            if self > angle {
                normalized -= increment
            } else {
                normalized += increment
            }
        }
        return normalized
    }
}
