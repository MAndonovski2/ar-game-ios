//
//  Float4x4+Extension.swift
//  Orlinski AR Game
//
//  Created by Martin on 31.8.20.
//  Copyright © 2020 Martin. All rights reserved.
//

import ARKit

extension float4x4 {
    var translationToSimdFloat3: simd_float3 {
        let translation = self.columns.3
        return simd_float3(translation.x, translation.y, translation.z)
    }
}
