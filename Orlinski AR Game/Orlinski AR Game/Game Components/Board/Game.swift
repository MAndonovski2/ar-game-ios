//
//  Game.swift
//  Orlinski AR Game
//
//  Created by Martin on 2.9.20.
//  Copyright © 2020 Martin. All rights reserved.
//

import Foundation

struct Game {
    enum SessionState {
        case setup
        case lookingForSurface
        case adjustingBoard
        case placingBoard
        case waitingForBoard
        case localizingToBoard
        case boardIsPlaced
        case sidePicking
        case setupStage
        case gameInProgress

        var localizedInstruction: String? {
            switch self {
            case .lookingForSurface:
                return NSLocalizedString("Find a flat surface to place the game", comment: "")
            case .placingBoard:
                return NSLocalizedString("Scale, rotate or move the board", comment: "")
            case .adjustingBoard:
                return NSLocalizedString("Make adjustments and tap to continue", comment: "")
            case .waitingForBoard:
                return NSLocalizedString("Synchronizing world map…", comment: "")
            case .localizingToBoard:
                return NSLocalizedString("Point the camera towards the table", comment: "")
            case .boardIsPlaced:
                return NSLocalizedString("Pick your side", comment: "")
            case .setup, .gameInProgress, .setupStage, .sidePicking:
                return nil
            }
        }
    }
}
